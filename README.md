# LFortran Website

# Build

Install [pixi](https://github.com/prefix-dev/pixi).

Initialize `hugo` submodule:

    git submodule update --init --recursive

To build the website locally do:

    pixi r hugo

This will build the website. To serve locally, do:

    pixi r hugo serve

The above works on Linux, macOS and Windows.
