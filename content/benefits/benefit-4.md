---
title: "Interactive"
icon: "fa fa-cogs"
---
LFortran allows to use Fortran interactively just like Python, Julia or MATLAB.
It works in a Jupyter notebook.
