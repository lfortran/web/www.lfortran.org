---
title: "LFortran compiles Fortran stdlib"
date: 2024-04-16
tags: ["Fortran", "Announcement", "stdlib", "fortran-lang"]
author: "[Ondřej Čertík](https://ondrejcertik.com/), [Gagandeep Singh](https://github.com/czgdp1807), [Ubaid Shaikh](https://github.com/Shaikh-Ubaid), [Thirumalai Shaktivel](https://www.linkedin.com/in/thirumalai-shaktivel/), [Pranav Goswami](https://www.linkedin.com/in/pranavgoswami1/), [Harshita Kalani](https://github.com/HarshitaKalani), [Parth Mistry](https://github.com/parth121101), [Gaurav Dhingra](https://github.com/gxyd), [Smit Lunagariya](https://www.linkedin.com/in/smit-lunagariya-356b93179/), [Luthfan Lubis](https://github.com/ansharlubis), [Naman Gera](https://uk.linkedin.com/in/namannimmo), [Zihua Wu](https://github.com/lucifer1004), [Khushi Agrawal](https://khushi-411.github.io/), [Christoph Junghans](https://www.linkedin.com/in/christophjunghans), [Brian Beckman](https://www.linkedin.com/in/brianbeckman), [Dylon Edwards](https://www.linkedin.com/in/dylon-edwards-0936bb39/)"
type: post
draft: false
---


# LFortran compiles 85% Fortran stdlib

We are happy to announce that LFortran can compile all numerical modules in `stdlib`. The `stdlib` library has 32 modules, here are the details of the current status:

* all 32 modules compile to ASR
* 27 modules (85%) compile to LLVM; there are 4 hash modules (`hash_32bit`, `hash_64bit`, `hashmaps`, `hashmap_wrappers`, `stringlist_type`) and one `stringlist_type` module that does not compile to LLVM yet
* 24 modules pass all their tests; 3 modules (`sorting`, `bitsets`, `stats`) do not pass all tests yet, but simple examples that use the modules work

This is sixth third-party production-grade code that LFortran can compile after [SciPy](https://lfortran.org/blog/2024/01/lfortran-compiles-60-of-scipy/). The [progress bar towards beta](https://lfortran.org) has thus reached 6/10.

All of numerical computing code compiles to LLVM and we also test this at CI for every LFortran commit, to ensure no regressions happen. The remaining module incorporates string handling and object oriented programming. Developers are currently working in these areas to make improvements. We also had to workaround several limitations by patching the `stdlib` code, and we opened a dedicated issue for each workaround.

LFortran is still alpha software, meaning that users must continue expecting that LFortran will fail compiling or running their codes. Please report all bugs that you find.

## stdlib Overview

[stdlib](https://github.com/fortran-lang/stdlib) provides a community driven and agreed upon de facto "standard" library for Fortran. It is both a specification and a reference implementation with goal to achieve utilities, algorithms, mathematics, etc.

The code implements a variety of numerical, array-based features to evaluate the efficacy and robustness of a compiler with respect to array operations.  While `SciPy` utilized many legacy features, `dftatom` and `fastGPT` had plenty modern fortran features, compilation of `stdlib` and other codebase emphasise the importance of progressing along all fronts and marks a significant advancement in its alpha-stage development, enhancing its usability for a diverse range of users.


## Development Phase Overview

Achieving this milestone proved comparably simpler than previous endeavors with the LFortran codebase. Our strategy entails a systematic compilation of third-party codes, foreseeing a smoother path towards beta with each successful compilation.

We initiated the process post-compiling SciPy, meticulously progressing module by module in a topologically sorted manner while concurrently working on test drive. Initially tackling straightforward tasks, we subsequently addressed more complex modules.

Our approach prioritized achieving full functionality within each module, resolving compilation failures, divergences, setting up CI, and executing essential changes, including refactoring functionalities.

We've encountered a notable challenge within a few lines of code, particularly regarding the [`submodules`](https://github.com/lfortran/lfortran/issues/536), which is already included in our roadmap for resolution. Additionally, minor workarounds have been implemented, as documented in the ongoing efforts outlined in the [`LFortran-stdlib-issues`](https://github.com/lfortran/lfortran/issues/536). We are actively addressing each of these issues to enhance the overall functionality and stability of the codebase.


## Test LFortran Compilation

LFortran can be used to compile and execute stdlib code using the following commands:

```
mamba create -n stdlib-tools fypp cmake make lfortran=0.35.0
mamba activate stdlib-tools
```

```
git clone https://github.com/czgdp1807/stdlib.git
cd stdlib
git checkout -t origin/lf19
git checkout e13b0117fb1cff549f7fc811daaddf3c40134db4
./build_test_lf.sh
```

`gfortran` 13.2.0 on MacBook Air M2 (2023):

```
% time make -j8
real	0m27.826s
user	1m52.294s
sys	0m8.928s
```

`lfortran` 0.35.0 on MacBook Air M2 (2023):

```
% time make -j8
real	0m5.688s
user	0m27.014s
sys	0m7.795s
```

## What’s Next?

As of this writing, LFortran compiles six third-party codes:

* [Legacy Minpack](https://github.com/scipy/scipy/tree/f797ac7721310c7bd98bae416be1bed9975b4203/scipy/optimize/minpack) (part of SciPy) and several more SciPy packages
* [Modern Minpack](https://github.com/fortran-lang/minpack)
* [fastGPT](https://github.com/certik/fastGPT)
* [dftatom](https://github.com/certik/dftatom)
* [SciPy](https://github.com/scipy/scipy) (60%)
* [stdlib](https://github.com/fortran-lang/stdlib) (85%) 

[Here](https://github.com/lfortran/lfortran/issues/3806) is our issue to track priorities to reach beta quality.

Our primary goal is to bring LFortran from alpha to beta and we think compiling 10 third-party codes is a way to achieve it. Our approach involves prioritizing features based on their importance, ensuring that LFortran supports all language features present in the selected codes. Progress towards beta will be measured by successfully compiling and running these codes without alterations. We will keep on announcing each one as LFortran is able to fully compile and run. The codes we are targeting on next are Fortran Package Manager (fpm), Lapack, SNAP, prima and remaining parts of Fortran stdlib and SciPy. Upon achieving the milestone of compiling 10 codes, we will collaborate with the community to identify any additional requirements for reaching beta status. A beta-quality compiler, in our context, is defined as one that executes user code without errors, though it may still contain bugs.


We continuously welcome new contributors to join our endeavor. If you're interested, please reach out to us. This is an exciting phase in the development of LFortran, as the process of compiling new codes is progressively becoming more accessible and enjoyable. Working on a compiler offers a stimulating learning experience, and we're committed to providing all the necessary guidance and training. Join us in shaping the future of LFortran!


## Acknowledgements

We want to thank:

* [Sovereign Tech Fund (STF)](https://sovereigntechfund.de/en/)
* [NumFOCUS](https://numfocus.org/)
* [QuantStack](https://quantstack.net/)
* [Google Summer of Code](https://summerofcode.withgoogle.com/)
* [GSI Technology](https://gsitechnology.com/)
* [LANL](https://lanl.gov/)
* Our GitHub, OpenCollective and NumFOCUS sponsors
* All our contributors (73 so far!)

## Discussions

* Fortran Discourse: https://fortran-lang.discourse.group/t/lfortran-compiles-stdlib/7855
* Twitter: https://twitter.com/lfortranorg/status/1780260738059116859
* Mastodon: https://mastodon.social/@certik/112282011911188565
* LinkedIn: https://www.linkedin.com/feed/update/urn:li:activity:7186024762924568578/
